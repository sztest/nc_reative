-- LUALOCALS < ---------------------------------------------------------
local ItemStack, ipairs, minetest, nodecore, pairs
    = ItemStack, ipairs, minetest, nodecore, pairs
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local function regnode(name, def)
	def.on_drop = function() return "" end
	def.on_place = function(stack, ...)
		local s = minetest.item_place(ItemStack(stack), ...)
		return s:is_empty() and stack or s
	end
	def.on_dig = function(a, b, digger, ...)
		if digger and digger:is_player()
		and minetest.check_player_privs(digger, modname) then
			return minetest.node_dig(a, b, digger, ...)
		end
	end
	def.virtual_item = true
	if def.liquidtype == "flowing" then
		def.drop = ""
	end
	return minetest.register_node(name, def)
end

local function cname(k)
	if not k then return k end
	if k:sub(1, 3) == "nc_" then k = k:sub(4) end
	return modname .. ":" .. k:gsub("%W", "_")
end

local deferred = {}
for k, v in pairs(minetest.registered_nodes) do
	if (not v.virtual_item) and (not v.groups.is_stack_only) then
		local def = {
			replica_original = v.name,
			description = "Replica " .. v.description,
			drawtype = v.drawtype,
			mesh = v.mesh,
			visual_scale = v.visual_scale,
			wield_scale = v.wield_scale,
			tiles = v.tiles,
			special_tiles = v.special_tiles,
			waving = v.waving,
			alpha = v.alpha,
			node_box = v.node_box,
			collison_box = v.collision_box,
			selection_box = v.selection_box,
			inventory_image = v.inventory_image,
			wield_image = v.wield_image,
			sounds = v.sounds,
			paramtype2 = v.paramtype2,
			paramtype = v.paramtype,
			light_source = v.light_source,
			sunlight_propagates = v.sunlight_propagates,
			groups = {snappy = 1},
			pointable = true,
			buildable_to = false,
			climbable = v.climbable,
			liquidtype = v.liquidtype,
			liquid_renewable = false,
			liquid_range = v.liquid_range,
			liquid_alternative_flowing = cname(v.liquid_alternative_flowing),
			liquid_alternative_source = cname(v.liquid_alternative_source)
		}
		if v.paramtype2 == "facedir" then
			def.on_rightclick = v.on_rightclick
		end
		deferred[#deferred + 1] = function()
			regnode(cname(k), def)
		end
	end
end
for _, v in ipairs(deferred) do v() end

for k, v in pairs(minetest.registered_aliases) do
	minetest.register_alias(cname(k), cname(v))
end

regnode(modname .. ":smoke", {
		replica_original = true,
		description = "Smoke Generator",
		drawtype = "airlike",
		inventory_image = "nc_api_craft_smoke.png",
		wield_image = "nc_api_craft_smoke.png",
		walkable = false,
		pointable = true,
		selection_box = nodecore.fixedbox({
				-0.4, -0.4, -0.4, 0.4, 0.4, 0.4
			}),
		paramtype = "light",
		sunlight_propagates = true,
		groups = {snappy = 1}

	})
nodecore.register_limited_abm({
		label = "Smoke Source",
		nodenames = {modname .. ":smoke"},
		interval = 1,
		chance = 1,
		action = function(pos)
			pos.y = pos.y - 1
			nodecore.smokefx(pos, 1)
		end
	})

regnode(modname .. ":light", {
		replica_original = true,
		description = "Light Generator",
		drawtype = "airlike",
		inventory_image = "nc_reative_light.png",
		wield_image = "nc_reative_light.png",
		walkable = false,
		pointable = true,
		selection_box = nodecore.fixedbox({
				-0.4, -0.4, -0.4, 0.4, 0.4, 0.4
			}),
		paramtype = "light",
		sunlight_propagates = true,
		light_source = 14,
		groups = {snappy = 1}

	})
