# Deprecated

The purpose of this mod was originally to help maintain the "screenshot world" used to make the official screenshots for NodeCore (and texture packs).  However, it has been superseded by a combination of:

- The `/stasis` feature build into NodeCore that allows freezing most things in time, allowing complex scenes to be staged without them evolving on their own.
- The use of `szutil_givemenu` from the [SzUtilPack](/packages/Warr1024/szutilpack/) modpack, which provides a much nicer searchable alternative to `/giveme` or `/rs`.
- A handful of worldmods in the screenshot world itself, such as deterministic smoke generators and player mannequins, which fulfill some of the non-node scene setup needs.

It's recommended that players consider using those features as well, instead of this package.

---

Adds "creative mode"-like functionality to NodeCore, mainly for screen-shots, mock-ups and the like.  Could also theoretically be used for administrative purposes (e.g. maintaining a spawn area).  **N.B.** It would probably be considered "cheating" to use of this mod in normal survival gameplay for uses other than these.

- Adds "replica" versions of most nodes that look like the originals, but are non-functioning.
- Adds an `nc_reative` priv necessary to access replica nodes (create or dig them).
- Adds `/rs` replica search command to create replicas by name.

Replica nodes...

- ...are always pointable, solid, and diggable by hand, but cannot be dug without the `nc_reative` priv.
- ...include liquids that will flow.
- ...are destroyed if you drop them.
- ...will never completely deplete a stack; the last one can be placed over and over.  Drop to free up the slot.
- ...can emit light, and can be hand-rotated (i.e. optics).

There is also a smoke generator for mocking up cooking recipe setups.