-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, table, tonumber
    = minetest, pairs, table, tonumber
local table_sort
    = table.sort
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local cmd = "rs"

minetest.register_privilege(modname, {
		description = "Can create replica nodes",
		give_to_singleplayer = false,
	})

local db = {}
for k, v in pairs(minetest.registered_nodes) do
	if v.liquidtype ~= "flowing" then
		if k:sub(1, #modname + 1) == modname .. ":" then
			for s in v.description
			:lower()
			:gsub("replica ", "")
			:gmatch("%w+") do
				local x = db[s]
				if not x then x = {} db[s] = x end
				x[k] = 1
			end
		end
	end
end

local results = {}

local function search(name, param)
	local u = {}

	local need = 0
	for s in param:lower():gmatch("%w+") do
		need = need + 1
		for k, v in pairs(db) do
			if k:find(s, 1, true) then
				for x in pairs(v) do
					u[x] = (u[x] or 0) + 1
				end
			end
		end
	end
	for k, v in pairs(u) do
		if v < need then
			u[k] = nil
		end
	end

	local t = {}
	for k in pairs(u) do
		local r = minetest.registered_nodes[k]
		local o = minetest.registered_nodes[r.replica_original] or r
		t[#t + 1] = {
			name = k,
			orig = o.name,
			desc = o.description
		}
	end
	if #t < 1 then
		return false, "No results found."
	end
	if #t == 1 then
		return minetest.registered_chatcommands
		.giveme.func(name, t[1].name)
	end
	while #t > 8 do t[#t] = nil end
	table_sort(t, function(a, b)
			if a.desc == b.desc then
				return a.orig < b.orig
			end
			return a.desc < b.desc
		end)

	results[name] = t
	for i, v in pairs(t) do
		minetest.chat_send_player(name, "/" .. cmd .. " " .. i
			.. " -> " .. v.desc .. " (" .. v.orig .. ")")
	end
end

minetest.register_chatcommand(cmd, {
		params = "<search>",
		description = "Replica node search",
		privs = {[modname] = true},
		func = function(name, param)
			local res = results[name]
			if res then
				local num = tonumber(param)
				if res[num] then
					return minetest.registered_chatcommands
					.giveme.func(name, res[num].name)
				end
			end
			return search(name, param)
		end
	})
