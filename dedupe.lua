-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs
    = minetest, pairs
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local prefix = modname .. ":"
minetest.register_globalstep(function()
		for _, pl in pairs(minetest.get_connected_players()) do
			local inv = pl:get_inventory()
			local seen = {}
			for i = 1, inv:get_size("main") do
				local stack = inv:get_stack("main", i)
				local name = stack:get_name()
				if name:sub(1, #prefix) == prefix then
					if seen[name] then
						inv:set_stack("main", i, "")
					elseif stack:get_count() > 1 then
						inv:set_stack("main", i, name)
					end
					seen[name] = true
				end
			end
		end
	end)
