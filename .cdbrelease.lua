-- LUALOCALS < ---------------------------------------------------------
local math, tonumber
    = math, tonumber
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local stamp = tonumber("$Format:%at$")
if not stamp then return end
stamp = math_floor((stamp - 1540612800) / 60)
stamp = ("00000000" .. stamp):sub(-8)

-- luacheck: push
-- luacheck: globals config readtext readbinary

readtext = readtext or function() end
readbinary = readbinary or function() end

return {
	user = "Warr1024",
	pkg = "nc_reative",
	type = "mod",
	dev_state = "DEPRECATED",
	tags = {
		"creative",
		"environment",
		"inventory",
		"world_tools"
	},
	license = "MIT",
	media_license = "MIT",
	long_description = readtext('README.md'),
	repo = "https://gitlab.com/sztest/nc_reative",
	forums = 24857,
	version = stamp .. "-$Format:%h$",
	screenshots = {readbinary(".cdbscreen.webp")}
}

-- luacheck: pop
